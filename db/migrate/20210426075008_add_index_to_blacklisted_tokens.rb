class AddIndexToBlacklistedTokens < ActiveRecord::Migration[6.1]
  def change
    add_index :blacklisted_tokens, :token, unique: true
  end
end
