class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.references :creator, null: false, foreign_key: { to_table: :users }
      t.references :parent, null: true, foreign_key: { to_table: :comments }
      t.text :body
      t.string :commentable_type
      t.integer :commentable_id

      t.timestamps
    end
  end
end
