Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: 'api/v1/posts#index'

  namespace :api, defaults: { format: 'json' } do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :posts, only: [:index, :show, :create, :update, :destroy], param: :slug do
        get 'comments', to: "comments#index"
        resources :comments, only:[:create, :update, :destroy]
      end

      post 'register', to: "users#register"
      post 'login', to: "users#login"
      post 'logout', to: "users#logout"
    end
  end


end
