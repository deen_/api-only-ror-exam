class JsonWebToken
  def self.encode(payload)
    # exp = Time.now.to_i + 4 * 3600
    # payload[:exp] = exp
    myid = Time.zone.now.strftime("%Y%d%m:%H%M%S")
    payload[:myid] = myid
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  def self.decode(token)
    HashWithIndifferentAccess.new(JWT.decode(token, Rails.application.secrets.secret_key_base)[0])
  end
end
