require "rails_helper"

RSpec.describe "Login", type: :request do
  let(:user) { create :user }
  let(:token) { JsonWebToken.encode(user_id: user.id) }
  let(:expiration) { Time.zone.now + 15 * 60 }

  it "logs in a valid user" do
    post "/api/login", params: { email: user.email, password: user.password }

    expect(response).to have_http_status(:ok)
    expect(response.body).to be_json_as(
      {
        "auth_token" => token,
        "token_type" => "bearer",
        "expires_at" => expiration.strftime("%F %T")
      }
    )
  end

  it "requires email and password to login" do
    post "/api/login", params: { email: user.email }
    expect(response).to have_http_status(:unprocessable_entity)

    post "/api/login", params: { password: user.password }
    expect(response).to have_http_status(:unprocessable_entity)

    post "/api/login", params: {}
    expect(response).to have_http_status(:unprocessable_entity)
  end
end
