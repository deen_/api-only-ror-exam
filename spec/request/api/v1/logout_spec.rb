require "rails_helper"

RSpec.describe "Logout", type: :request do
  let(:user) { create :user }
  let(:token) { generate_token user.id }

  it "logs out a valid user" do
    post "/api/logout", headers: token

    expect(response).to have_http_status(:ok)
    expect(response.body).to be_json_as(
      {
        message: "Successfully logged out"
      }
    )
  end
end
