require "rails_helper"

def format_created_comment(posts, user)
  {
    "id"               => Comment.last.id,
    "creator_id"       => user.id,
    "parent_id"        => nil,
    "body"             => "Sample comment",
    "commentable_type" => "Post",
    "commentable_id"   => posts.id,
    "updated_at"       => date_now,
    "created_at"       => date_now
  }
end

RSpec.describe "Post/Comments API", type: :request do
  let(:posts) { create :post }
  let(:user) { create :user }
  let(:token) { generate_token user.id }
  let(:comment_params) do
    { comment: { body: "Sample comment" } }
  end
  let(:date_now) { Time.zone.now.strftime("%Y-%m-%d %H:%M:%S") }

  describe "POST /api/posts/{post}/comments" do
    it "creates a new comment to post" do
      post "/api/posts/#{posts.slug}/comments", params: comment_params, headers: token

      expect(response).to have_http_status(:created)
      expect(response.body).to be_json_as(
        { data: format_created_comment(posts, user) }
      )
    end

    it "cannot create new comment if unauthenticated" do
      post "/api/posts/#{posts.slug}/comments", params: comment_params, headers: {}
      expect(response).to have_http_status(:unauthorized)
    end

    it "will raise error for empty params" do
      post "/api/posts/#{posts.slug}/comments", params: {}, headers: token
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
