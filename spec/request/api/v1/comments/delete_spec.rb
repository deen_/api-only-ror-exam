require "rails_helper"

RSpec.describe "Post/Comments API", type: :request do
  let(:posts) { create :post }
  let(:token) { generate_token posts.user_id }
  let(:comment) { posts.comments.first } # post owner comments created at post factory

  describe "DELETE /api/posts/{post}/comments/{comment}" do
    it "deletes own comment to post" do
      delete "/api/posts/#{posts.slug}/comments/#{comment.id}", headers: token

      expect(response).to have_http_status(:ok)
      expect(response.body).to be_json_as(
        {
          status: "record deleted successfully"
        }
      )
    end

    it "cannot delete comment if unauthenticated" do
      delete "/api/posts/#{posts.slug}/comments/#{comment.id}", headers: {}
      expect(response).to have_http_status(:unauthorized)
    end

    it "cannot delete comment if unauthorized (not the owner of comment)" do
      delete "/api/posts/#{posts.slug}/comments/#{comment.id}",
             headers: { "Authorization" => "Bearer SOME-OTHER-TOKEN" }
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
