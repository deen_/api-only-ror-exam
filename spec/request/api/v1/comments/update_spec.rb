require "rails_helper"

def format_update_comment(comment)
  comment["body"] = "Updated comment"
  comment["created_at"] = comment["created_at"].strftime("%Y-%m-%d %H:%M:%S")
  comment["updated_at"] = Time.zone.now.strftime("%Y-%m-%d %H:%M:%S")
  comment.attributes
end

RSpec.describe "Post/Comments API", type: :request do
  let(:posts) { create :post }
  let(:token) { generate_token posts.user_id }
  let(:comment) { posts.comments.first } # post owner comments created at post factory
  let(:comment_params) do
    { comment: { body: "Updated comment" } }
  end

  describe "PATCH /api/posts/{post}/comments/{comment}" do
    it "updates own comment to post" do
      patch "/api/posts/#{posts.slug}/comments/#{comment.id}", params:  comment_params,
                                                               headers: token
      expect(response).to have_http_status(:ok)
      expect(response.body).to be_json_as({ "data" => format_update_comment(comment) })
    end

    it "cannot update comment if unauthorized (not the owner of comment)" do
      patch "/api/posts/#{posts.slug}/comments/#{comment.id}",
            params:  comment_params,
            headers: { "Authorization" => "Bearer SOME-OTHER-TOKEN" }
      expect(response).to have_http_status(:unauthorized)
    end

    it "will raise error for empty params" do
      patch "/api/posts/#{posts.slug}/comments/#{comment.id}", params:  {},
                                                               headers: token
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
