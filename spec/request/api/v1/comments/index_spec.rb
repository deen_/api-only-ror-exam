require "rails_helper"

RSpec.describe "Post/Comments API", type: :request do
  let(:post) { create :post } # creates 3 comments

  describe "GET /api/posts/{post}/comments" do
    it "returns all post comments" do
      get "/api/posts/#{post.slug}/comments"

      comments = post.comments.map do |c|
        c["created_at"] = c["created_at"].strftime("%Y-%m-%d %H:%M:%S")
        c["updated_at"] = c["updated_at"].strftime("%Y-%m-%d %H:%M:%S")
        c.attributes
      end

      expect(response).to have_http_status(:ok)
      expect(Comment.count).to eq(3)
      expect(response.body).to be_json_as(
        {
          "data" => comments
        }
      )
    end

    it "returns 404 error for getting comments of unexisting post" do
      get "/api/posts/not-exist/comments"

      expect(response).to have_http_status(:not_found)
    end
  end
end
