require "rails_helper"

def format_update_post(post)
  post["created_at"] = post["created_at"].strftime("%Y-%m-%d %H:%M:%S")
  post["updated_at"] = Time.zone.now.strftime("%Y-%m-%d %H:%M:%S")
  post["content"] = "Updated content"
  post.attributes
end

RSpec.describe "Posts API", type: :request do
  let(:post) { create :post }
  let(:token) { generate_token post.user_id }
  let(:post_params) do
    { post: { content: "Updated content" } }
  end

  describe "PATCH /posts/{post}" do
    it "updates own existing post " do
      patch "/api/posts/#{post.slug}", params: post_params, headers: token

      expect(response).to have_http_status(:ok)
      expect(response.body).to be_json_as({ "data" => format_update_post(post) })
    end

    it "cannot update post if unauthenticated" do
      patch "/api/posts/#{post.slug}", params: post_params, headers: {}
      expect(response).to have_http_status(:unauthorized)
    end

    it "cannot update post if unauthorized (not the owner of post)" do
      patch "/api/posts/#{post.slug}", params:  post_params,
                                       headers: { "Authorization" => "Bearer SOME-OTHER-TOKEN" }

      expect(response).to have_http_status(:unauthorized)
    end

    it "will raise error for empty params" do
      patch "/api/posts/#{post.slug}", params: {}, headers: token
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
