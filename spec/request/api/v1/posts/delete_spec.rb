require "rails_helper"

RSpec.describe "Posts API", type: :request do
  let(:post) { create :post }
  let(:token) { generate_token post.user_id }

  describe "DELETE /posts/{post}" do
    it "deletes own existing post " do
      delete "/api/posts/#{post.slug}", headers: token

      expect(response).to have_http_status(:ok)
      expect(response.body).to be_json_as(
        {
          status: "record deleted successfully"
        }
      )
    end

    it "cannot delete post if unauthenticated" do
      delete "/api/posts/#{post.slug}", headers: {}

      expect(response).to have_http_status(:unauthorized)
    end

    it "cannot delete post if unauthorized (not the owner of post)" do
      delete "/api/posts/#{post.slug}", headers: { "Authorization" => "Bearer SOME-OTHER-TOKEN" }

      expect(response).to have_http_status(:unauthorized)
    end
  end
end
