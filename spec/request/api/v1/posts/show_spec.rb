require "rails_helper"

RSpec.describe "Posts API", type: :request do
  let(:post) { create :post }

  describe "GET /posts/{post}" do
    it "returns details of existing post " do
      post["created_at"] = post["created_at"].strftime("%Y-%m-%d %H:%M:%S")
      post["updated_at"] = post["updated_at"].strftime("%Y-%m-%d %H:%M:%S")

      get "/api/posts/#{post.slug}"

      expect(response).to have_http_status(:ok)
      expect(response.body).to be_json_as(
        {
          "data" => post.attributes
        }
      )
    end

    it "returns 404 error for an unexisting post" do
      get "/api/posts/not-exist"

      expect(response).to have_http_status(:not_found)
    end
  end
end
