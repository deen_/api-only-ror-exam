require "rails_helper"

def qwe(posts)
  posts.map do |c|
    c["created_at"] = c["created_at"].strftime("%Y-%m-%d %H:%M:%S").to_s
    c["updated_at"] = c["updated_at"].strftime("%Y-%m-%d %H:%M:%S")
    c.attributes
  end
end

links =
  {
    "first" => "http://www.example.com/api/posts?page=1",
    "last"  => "http://www.example.com/api/posts?page=1",
    "prev"  => nil,
    "next"  => nil
  }

meta =
  {
    "current_page" => 1,
    "from"         => 1,
    "last_page"    => 1,
    "path"         => "http://www.example.com/api/posts",
    "per_page"     => 2,
    "to"           => 2,
    "total"        => 2
  }

RSpec.describe "Posts API", type: :request do
  describe "GET /posts" do
    it "returns all posts" do
      posts = create_list(:post, 2)
      # post_list = qwe(posts)
      post_list = posts.map do |p|
        p["created_at"] = p["created_at"].strftime("%Y-%m-%d %H:%M:%S")
        p["updated_at"] = p["updated_at"].strftime("%Y-%m-%d %H:%M:%S")
        p.attributes
      end

      get "/api/posts"

      expect(response).to have_http_status(:success)
      expect(response.body).to be_json(
        "data"  => post_list,
        "links" => links,
        "meta"  => meta
      )
    end
  end
end
