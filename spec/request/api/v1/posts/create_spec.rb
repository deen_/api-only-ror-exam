require "rails_helper"

def format_created_post(user)
  {
    "title"      => "Post",
    "content"    => "Sample content",
    "slug"       => "post",
    "user_id"    => user.id,
    "updated_at" => date_now,
    "created_at" => date_now,
    "id"         => Post.last.id
  }
end

RSpec.describe "Posts API", type: :request do
  let(:user) { create :user }
  let(:token) { generate_token user.id }
  let(:post_params) do
    { post: { title: "Post", slug: "post", content: "Sample content" } }
  end
  let(:date_now) { Time.zone.now.strftime("%Y-%m-%d %H:%M:%S") }

  describe "POST /posts" do
    it "creates a new post" do
      post "/api/posts", params: post_params, headers: token

      expect(response).to have_http_status(:created)
      expect(response.body).to be_json_as(
        {
          data: format_created_post(user)
        }
      )
    end

    it "cannot create new post if unauthenticated" do
      post "/api/posts", params: post_params, headers: {}
      expect(response).to have_http_status(:unauthorized)
    end

    it "will raise error for empty params" do
      post "/api/posts", params: {}, headers: token
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
