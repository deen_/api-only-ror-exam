require "rails_helper"
require "webmock/rspec"
WebMock.disable_net_connect!(allow_localhost: true)

RSpec.describe "Posts API", type: :request do
  let(:posts) { create :post }
  let(:token) { generate_token posts.user_id }
  let(:base64_image) { File.read("./spec/image/base64_image.txt") }
  let(:post_params) do
    { post: { title: "Post with Image", slug: "post-with-image",
content: "Sample content with image", image: base64_image } }
  end
  let(:post_update_params) do
    { post: { image: base64_image } }
  end

  describe "Uploading image" do
    it "save url to image field in post create" do
      stub_request(:any, /api.cloudinary.com/).to_return(body: { url: "fake_url" }.to_json)
      post "/api/posts", params: post_params, headers: token
      created_post = Post.last

      expect(created_post.image).to eq("fake_url")
    end

    it "save url to image field in post update" do
      stub_request(:any, /api.cloudinary.com/).to_return(body: { url: "fake_url" }.to_json)
      patch "/api/posts/#{posts.slug}", params: post_update_params, headers: token
      updated_post = Post.find_by(id: posts.id)

      expect(updated_post.image).to eq("fake_url")
    end
  end
end
