require "rails_helper"

RSpec.describe "Register", type: :request do
  let(:user_params) do
    { user: { name: "Deen", email: "deen@example.com", password: "password",
password_confirmation: "password" } }
  end
  let(:date_now) { Time.zone.now.strftime("%Y-%m-%d %H:%M") }

  it "can create a new user" do
    post "/api/register", params: user_params

    expect(response).to have_http_status(:created)
    expect(response.body).to be_json_including(
      {
        "name"  => "Deen",
        "email" => "deen@example.com"
      }
    )
  end

  it "cannot create new user without email and password" do
    post "/api/register",
         params: { user: { name: "Deen", email: "", password: "", password_confirmation: "" } }

    expect(response).to have_http_status(:unprocessable_entity)
  end

  it "cannot create new user with empty params" do
    post "/api/register", params: {}

    expect(response).to have_http_status(:unprocessable_entity)
  end
end
