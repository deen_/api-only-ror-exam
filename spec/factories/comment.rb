FactoryBot.define do
  factory :comment do
    body { FFaker::Lorem.characters(20) }
    commentable_type { "Post" }
    user
  end
end
