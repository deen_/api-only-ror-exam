FactoryBot.define do
  factory :post do
    title { FFaker::Lorem.word }
    slug { FFaker::Internet.slug(title, "-") }
    content { FFaker::Lorem.characters(20) }

    user

    after :create do |post|
      post.comments = create_list :comment, 3, commentable_id: post.id, creator_id: post.user_id
    end
  end
end
