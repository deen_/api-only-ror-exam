FactoryBot.define do
  factory :user do
    name { FFaker::Name.unique.name }
    email { FFaker::Internet.email }
    password { "password" }
    password_confirmation { "password" }
  end
end
