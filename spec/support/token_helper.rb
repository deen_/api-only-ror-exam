module TokenGenerator
  def generate_token(id)
    token_new = JsonWebToken.encode(user_id: id)
    { "Authorization" => "Bearer #{token_new}" }
  end
end

RSpec.configure do |c|
  c.include TokenGenerator
end
