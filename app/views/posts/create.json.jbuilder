json.data do
  json.title @post.title
  json.content @post.content
  json.slug @post.slug
  json.updated_at @post.updated_at
  json.created_at @post.created_at
  json.id @post.id
  json.user_id @post.user_id
end