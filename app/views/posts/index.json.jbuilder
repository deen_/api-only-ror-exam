json.data @posts

json.links do
  json.first @url + '?page=1'
  json.last @url + '?page=' + @posts.total_pages.to_s

  if @posts.prev_page
    json.prev @url + '?page=' + @posts.prev_page.to_s
  else
    json.prev @posts.prev_page
  end

  if @posts.next_page
    json.next @url + '?page=' + @posts.next_page.to_s
  else
    json.next @posts.next_page
  end
end

@from = (@posts.current_page.to_i * @per_page) - @per_page 
json.meta do
  json.current_page @posts.current_page

  if @posts.count > 0
    json.from @from + 1
  else
    json.from 1
  end
  
  json.last_page @posts.total_pages
  json.path @url
  json.per_page @posts.size
  json.to @from + @posts.count
  json.total @posts.total_count
end