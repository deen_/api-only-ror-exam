class User < ApplicationRecord
  has_secure_password
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, presence: true

  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy, foreign_key: "creator_id", inverse_of: :user
  has_many :blacklisted_tokens, dependent: :destroy
end
