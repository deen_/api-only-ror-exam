class Comment < ApplicationRecord
  validates :body, presence: true

  belongs_to :user, foreign_key: "creator_id", inverse_of: :comments
  belongs_to :commentable, polymorphic: true
  # belongs_to :comment, :foreign_key => 'parent_id'

  def publish(user)
    self.creator_id = user.id
    save
  end
end
