class Post < ApplicationRecord
  validates :title, presence: true
  validates :slug, presence: true
  validates :content, presence: true

  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy
  acts_as_paranoid

  def publish
    self.slug = title.parameterize if title

    if image
      upload_image
      self.image = @image["url"]
    else
      self.image = nil
    end
    save
  end

  def upload_image
    base64 = image
    @image = Cloudinary::Uploader.upload(base64)
  end
end
