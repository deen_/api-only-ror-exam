class Api::V1::CommentsController < Api::V1::BaseController
  rescue_from ActionController::ParameterMissing, with: :require_comment_param
  before_action :authenticate_request!, only: %i[create update destroy]
  before_action :set_comment, only: %i[update destroy]
  before_action :set_post, only: %i[create]

  def index
    @post = Post.find_by(slug: params[:post_slug])
    if @post
      render json: { data: @post.comments }, status: :ok
    else
      render json: { message: "Post not found" }, status: :not_found
    end
  end

  def create
    render json: { message: "Post not found" }, status: :not_found if @post.nil?

    return unless @post

    @comment = @post.comments.build(comments_params)
    @comment.publish(current_user)
    render json: { data: @comment }, status: :created
  end

  def update
    if @comment
      @comment.update comments_params
      render json: { data: @comment }, status: :ok
    else
      render json: unauthorized_msg("comment"), status: :unauthorized
    end
  end

  def destroy
    if @comment
      @comment.delete
      render json: { status: "record deleted successfully" }, status: :ok
    else
      render json: unauthorized_msg("comment"), status: :unauthorized
    end
  end

  private

  def comments_params
    params.require(:comment).permit(:body)
  end

  def set_comment
    @comment = current_user.comments.find_by(id: params[:id])
  end

  def set_post
    @post = Post.find_by(slug: params[:post_slug])
  end

  def require_comment_param
    @errors = {}
    @errors[:body] = ["The body field is required."]
    render "shared/error", formats: :json, status: :unprocessable_entity
  end
end
