class Api::V1::BaseController < ApplicationController
  attr_reader :current_user

  include ApiErrorHandler

  protected

  def authenticate_request!
    unless user_id_in_token?
      render json: { errors: ["Not Authenticated"] }, status: :unauthorized
      return
    end
    @current_user = User.find(auth_token[:user_id])
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ["Not Authenticated"] }, status: :unauthorized
  end

  private

  def http_token
    @http_token ||= if request.headers["Authorization"].present?
                      request.headers["Authorization"].split.last
                    end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def user_id_in_token?
    http_token && auth_token && auth_token[:user_id].to_i && not_blacklisted?
  end

  def unauthorized_msg(model)
    {
      error: "Unauthorized! You are not the owner of this #{model}."
    }
  end

  def not_blacklisted?
    user = User.find(auth_token[:user_id])
    blacklisted = user.blacklisted_tokens.find_by(token: @http_token)
    blacklisted.nil?
  end
end
