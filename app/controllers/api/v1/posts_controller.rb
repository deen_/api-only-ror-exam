class Api::V1::PostsController < Api::V1::BaseController
  rescue_from ActionController::ParameterMissing, with: :require_post_params
  before_action :authenticate_request!, only: %i[create update destroy]
  before_action :set_post, only: %i[update destroy]

  # api :GET, "/posts", "Posts List"
  def index
    @per_page = 15
    @posts = Post.page(params[:page]).per(@per_page)
    @url = request.base_url + request.path

    render "posts/index", formats: :json
  end

  def show
    @post = Post.find_by(slug: params[:slug])
    if @post
      render json: { data: @post }, status: :ok
    else
      render json: { message: "Post not found" }, status: :not_found
    end
  end

  def create
    @post = current_user.posts.build(post_params)
    @post.publish
    if @post.valid?
      render "posts/create", formats: :json, status: :created
    else
      @errors = @post.errors
      render "shared/error", formats: :json, status: :unprocessable_entity
    end
  rescue ActiveRecord::RecordNotUnique
    render json: { message: "Slug should be unique." }, status: :unprocessable_entity
  end

  def update
    if @post
      @post.update post_params
      @post.publish
      render json: { data: @post }, status: :ok
    else
      render json: unauthorized_msg("post"), status: :unauthorized
    end
  end

  def destroy
    if @post
      @post.delete
      render json: { status: "record deleted successfully" }, status: :ok
    else
      render json: unauthorized_msg("post"), status: :unauthorized
    end
  end

  private

  def post_params
    params.require(:post).permit(:title, :content, :image)
  end

  def set_post
    @post = current_user.posts.find_by(slug: params["slug"])
  end

  def require_post_params
    @errors = {}
    @errors[:title] = ["The title field is required."]
    @errors[:content] = ["The content field is required."]
    render "shared/error", formats: :json, status: :unprocessable_entity
  end
end
