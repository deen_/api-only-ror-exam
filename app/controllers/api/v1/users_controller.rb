class Api::V1::UsersController < Api::V1::BaseController
  rescue_from ActionController::ParameterMissing, with: :require_user_params
  before_action :authenticate_request!, only: %i[logout]
  before_action :require_login_params, only: %i[login]
  wrap_parameters :user, include: %i[name email password password_confirmation]

  def register
    @user = User.create(user_params)
    if @user.valid?
      render "users/register", formats: :json, status: :created
    else
      @errors = @user.errors
      render "shared/error", formats: :json, status: :unprocessable_entity
    end
  end

  def login
    @user = User.find_by(email: params[:email])
    if @user&.authenticate(params[:password])
      render json: payload(@user), status: :ok
    else
      @errors[:email] = ["These credentials do not match our records."] if params.key?(:email)
      render "shared/error", formats: :json, status: :unprocessable_entity
    end
  end

  def logout
    current_user.blacklisted_tokens.create(token: @http_token)
    render json: { message: "Successfully logged out" }, status: :ok
  rescue ActiveRecord::RecordNotUnique
    render json: { message: "Successfully logged out" }, status: :ok
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def require_login_params
    @errors = {}
    @errors[:email] = ["The email field is required."] unless params.key?(:email)
    @errors[:password] = ["The password field is required."] unless params.key?(:password)
  end

  def payload(user)
    return nil unless user&.id

    token = JsonWebToken.encode({ user_id: user.id }) # expires in 15 minutes
    blacklisted = @user.blacklisted_tokens.find_by(token: token)

    blacklisted&.delete

    expiration = Time.zone.now + 15 * 60
    {
      auth_token: token,
      token_type: "bearer",
      expires_at: expiration.strftime("%F %T")
    }
  end

  def require_user_params
    @errors = {}
    @errors[:name] = ["The name field is required."]
    @errors[:email] = ["The email field is required."]
    @errors[:password] = ["The password field is required."]
    render "shared/error", formats: :json, status: :unprocessable_entity
  end
end
