module ApiErrorHandler
  extend ActiveSupport::Concern
  included do
    rescue_from ActionController::ParameterMissing do |e|
      Rails.logger.error(e)
      Rails.logger.error(e.backtrace.join("\n"))
      render json: { message: e.message }, status: :bad_request
    end
  end
end
